﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ELLI.ViewModels
{
	public class BaseViewModel : INotifyPropertyChanged
	{

		protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null) {
			if (Object.Equals(storage, value))
				return false;
			storage = value;
			OnPropertyChanged(propertyName);
			return true;
		}

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, 
					new PropertyChangedEventArgs(propertyName));
			}
		}

		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion
	}
}

