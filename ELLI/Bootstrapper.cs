﻿using System;
using SourceFile.IoC;
using SourceFile.IoC.UI.Interfaces;
using SourceFile.IoC.UI;
using ELLI.ViewModels;
using ELLI.Views;

namespace ELLI
{
	public class Bootstrapper
	{
		public static void Setup(){

			/**
			 * Repositories
			 */
//			Ioc.RegisterSingleton <IProductRepository, ProductRepository> ();

			/**
             * Services
             */
			Ioc.RegisterSingleton<INavigationService, NavigationService>();

			//Main
			Ioc.RegisterView<MainViewModel, MainView> ();


		}
	}
}

