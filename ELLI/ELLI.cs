﻿using System;

using Xamarin.Forms;

namespace ELLI
{
	public class App : Application
	{
		public App (Lazy<Page> startPage)
		{
			Bootstrapper.Setup ();

			MainPage = startPage.Value;
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

