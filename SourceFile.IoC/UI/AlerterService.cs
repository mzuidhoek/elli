﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using SourceFile.IoC.UI.Interfaces;

namespace SourceFile.IoC.UI
{
	/// <inheritdoc/>
	public class AlerterService : IAlerterService
	{
		#region IAlerterService implementation

		/// <inheritdoc/>
		public async Task<bool> DisplayAlert (object currentVm, string title, string message, string cancel)
		{
			Page currentPage = 
				currentVm == null ? Application.Current.MainPage : FindPageForBindingContext(currentVm);

			if (currentPage != null) {
				return await (Task<bool>)currentPage.DisplayAlert(title,message,cancel);
			}
			return false;
		}

		/// <inheritdoc/>
		public async Task<bool> DisplayAlert (object currentVm, string title, string message, string accept, string cancel)
		{
			Page currentPage = 
				currentVm == null ? Application.Current.MainPage : FindPageForBindingContext(currentVm);

			if (currentPage != null) {
				return await currentPage.DisplayAlert(title,message,accept,cancel);
			}
			return false;
		}

		#endregion

		Page FindPageForBindingContext(object bindingContext) {
			var result = 
				EnumerateBreadthFirst(Application.Current.MainPage)
					.FirstOrDefault(e => ReferenceEquals(e.BindingContext, bindingContext))
					as Page;

			if (result == null)
				throw new InvalidOperationException(
					string.Format("No Xamarin Forms page found in hierarchy for view model {0}" , bindingContext)
				);
			return result;
		}

		#region Utils
		static IEnumerable<Element> EnumerateBreadthFirst(Element el) {
			IEnumerable<Element> generation = new[] { el };
			bool isGenerationEmpty;
			do {
				isGenerationEmpty = true;
				IEnumerable<Element> nextGen = Enumerable.Empty<Element>();
				foreach (Element c in generation) {
					yield return c;
					nextGen = nextGen.Concat(EnumerateChildren(c));
					isGenerationEmpty = false;
				}
				generation = nextGen;
			} while (!isGenerationEmpty);
		}

		static IEnumerable<Element> EnumerateChildren(Element el) {
			var contentPage = el as ContentPage;
			if (contentPage != null) {
				yield return contentPage.Content;
				yield break;
			}

			var masterDetailPage = el as MasterDetailPage;
			if (masterDetailPage != null) {
				yield return masterDetailPage.Master;
				yield return masterDetailPage.Detail;
				yield break;
			}

			var pageContainer = el as IPageContainer<Page>;
			if (pageContainer != null) {
				yield return pageContainer.CurrentPage;
				yield break;
			}

			var viewContainer = el as IViewContainer<View>;
			if (viewContainer != null) {
				foreach (View v in viewContainer.Children)
					yield return v;
				yield break;
			}
		}
		#endregion
	}
}

