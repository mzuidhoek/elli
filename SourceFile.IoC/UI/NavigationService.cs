﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Linq;
using SourceFile.IoC.UI.Interfaces;
using SourceFile.IoC.ServiceInterfaces;

namespace SourceFile.IoC.UI
{
	/// <inheritdoc/>
	public class NavigationService : INavigationService, INeedInitialization
	{

		IResolverService _resolverService;
		Stack<Page> _pageStack = new Stack<Page>();

		public NavigationService(IResolverService resolverService)
		{
			_resolverService = resolverService;
		}


		#region INeedInitialization implementation

		public void Initialize ()
		{
			_pageStack.Push (Application.Current.MainPage);
			Application.Current.ModalPushed += HandleModalPushed;
			Application.Current.ModalPopped += HandleModalPopped;
		}

		#endregion

		Page CurrentPage { 
			get {
				return _pageStack.Peek ();
			}
		}

		void HandleModalPushed (object sender, ModalPushedEventArgs e)
		{
			_pageStack.Push (e.Modal);
		}

		void HandleModalPopped (object sender, ModalPoppedEventArgs e)
		{
			_pageStack.Pop ();
		}

		#region INavigationService implementation

		/// <inheritdoc/>
		public TViewModel CreateViewModel<TViewModel>() 
			where TViewModel : class
		{
			return _resolverService.Resolve<TViewModel>();
		}



		/// <inheritdoc/>
		public object CreateView<TViewModel>(TViewModel viewModel = null) 
			where TViewModel : class
		{
			return CreatePageWithBindingContext(viewModel ?? CreateViewModel<TViewModel>());
		}

		/// <inheritdoc/>
		public async Task<object> PushAsync<TToVm>(object currentVm, TToVm toVm = null, string animation = "", bool modal = false)
			where TToVm : class
		{
			Page fromPage = 
				currentVm == null ? CurrentPage : FindPageForBindingContext(currentVm);

			toVm = toVm ?? CreateViewModel<TToVm>();

			Page toPage = CreatePageWithBindingContext<TToVm>(toVm);
			if (modal)
				await fromPage.Navigation.PushModalAsync(toPage, animation != null);
			else
				await fromPage.Navigation.PushAsync(toPage, animation != null);

			return toVm;
		}

		/// <inheritdoc/>
		public async Task<object> PopAsync(object currentVm, string animation = "", bool modal = false)
		{
			Page fromVisualElement = 
				currentVm == null ? CurrentPage : FindPageForBindingContext(currentVm);

			Page newPage;
			if (modal)
				newPage = await fromVisualElement.Navigation.PopModalAsync (animation != null);
			else
				newPage = await fromVisualElement.Navigation.PopAsync(animation != null);

			return newPage.BindingContext;
		}

		/// <inheritdoc/>
		public void ReduceBackStackTo<TVm>(object currentVm, TVm viewModel = null, bool inclusive = false) where TVm : class
		{
			Page fromPage = 
				currentVm == null ? CurrentPage : FindPageForBindingContext(currentVm);

			var navStack = fromPage.Navigation.NavigationStack;

			var toRemove = new List<Page>();
			for (int i = navStack.Count - 2; i > 0; i--) {
				Page cur = navStack[i];
				if (inclusive) toRemove.Add(cur);
				if (viewModel == null 
					? cur.BindingContext is TVm 
					: ReferenceEquals(cur.BindingContext, viewModel)) break;
				if (!inclusive) toRemove.Add(cur);
			}

			foreach (Page cur in toRemove)
				fromPage.Navigation.RemovePage(cur);
		}


		Page FindPageForBindingContext(object bindingContext) {
			var result = 
				EnumerateBreadthFirst(CurrentPage)
					.FirstOrDefault(e => ReferenceEquals(e.BindingContext, bindingContext))
					as Page;

			if (result == null)
				throw new InvalidOperationException(
					string.Format("No Xamarin Forms element found in hierarchy for view model {0}" , bindingContext)
				);
			return result;
		}

		Page CreatePageWithBindingContext<TBindingContext>(TBindingContext bindingContext) {
			var result = 
				_resolverService.ResolveView(typeof(TBindingContext)) as Page;

			if (result == null)
				throw new InvalidOperationException(
					string.Format("No Xamarin Forms Page Type registered for view model type {0}", typeof(TBindingContext))
				);
			result.BindingContext = bindingContext;
			return result;
		}

		#endregion

		static IEnumerable<Element> EnumerateBreadthFirst(Element el) {
			IEnumerable<Element> generation = new[] { el };
			bool isGenerationEmpty;
			do {
				isGenerationEmpty = true;
				IEnumerable<Element> nextGen = Enumerable.Empty<Element>();
				foreach (Element c in generation) {
					if (c != null)
						yield return c;
					nextGen = nextGen.Concat(EnumerateChildren(c));
					isGenerationEmpty = false;
				}
				generation = nextGen;
			} while (!isGenerationEmpty);
		}

		static IEnumerable<Element> EnumerateChildren(Element el) {
			var contentPage = el as ContentPage;
			if (contentPage != null) {
				yield return contentPage.Content;
				yield break;
			}

			var masterDetailPage = el as MasterDetailPage;
			if (masterDetailPage != null) {
				yield return masterDetailPage.Master;
				yield return masterDetailPage.Detail;
				yield break;
			}

			var pageContainer = el as IPageContainer<Page>;
			if (pageContainer != null) {
				yield return pageContainer.CurrentPage;
				yield break;
			}

			var viewContainer = el as IViewContainer<View>;
			if (viewContainer != null) {
				foreach (View v in viewContainer.Children)
					yield return v;
				yield break;
			}
		}

	}
}

