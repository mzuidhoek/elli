﻿using System;
using System.Threading.Tasks;

namespace SourceFile.IoC.UI.Interfaces
{
	public interface IAlerterService
	{
		Task<bool> DisplayAlert(object currentVm, string title, string message, string cancel);

		Task<bool> DisplayAlert(object currentVm, string title, string message, string accept, string cancel);

	}
}

