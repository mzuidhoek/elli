﻿using System;
using System.Threading.Tasks;

namespace SourceFile.IoC.UI.Interfaces
{
	public interface INavigationService
	{
		TViewModel CreateViewModel<TViewModel>() where TViewModel : class;

		object CreateView<TViewModel>(TViewModel viewModel = null) where TViewModel : class;

		Task<object> PushAsync<TToVm>(object currentVm, TToVm toVm = null, string animation = "", bool modal = false) where TToVm : class;

		Task<object> PopAsync(object currentVm, string animation = "", bool modal = false);

		void ReduceBackStackTo<TVm>(object currentVm, TVm viewModel = null, bool inclusive = false) where TVm: class;
	}
}

