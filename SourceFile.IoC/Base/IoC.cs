﻿using System;
using SourceFile.IoC.ServiceInterfaces;
using Autofac;
using System.Collections.Generic;

namespace SourceFile.IoC
{
	public static class Ioc
	{
		static IResolverService _resolver = null;
		static Dictionary<Type, Type> _vmToViewMap = new Dictionary<Type, Type>();

		static ContainerBuilder _builder;
		static IContainer _container;
		static ILifetimeScope _scope;

		static Ioc() 
		{
			Reset();
		}

		static ILifetimeScope Scope {
			get {
				if (_scope == null) {
					if (_container == null) {
						_container = Builder.Build();
					}
					_scope = _container.BeginLifetimeScope();
				}
				return _scope;
			}
		}

		static ContainerBuilder Builder {
			get {
				if (_builder == null) {
					_builder = new ContainerBuilder();
				}
				return _builder;
			}
		}

		static IResolverService Resolver {
			get {
				if (_resolver == null)
					_resolver = Scope.Resolve<IResolverService>();
				return _resolver;
			}
		}

		public static bool IsRegistering {
			get { return _container == null; }
		}

		public static void RegisterSingleton<TSvc, TImpl>(Dictionary<string, object> parameters = null) 
			where TSvc : class
			where TImpl : class, TSvc
		{
			if (parameters == null)
				parameters = new Dictionary<string, object>();

			// register the service
			var reg = Builder.RegisterType<TImpl>().As<TSvc>();

			// make sure that, if life cycle service is also exposed, it is registered as well
			if (typeof(TImpl).IsAssignableTo<INeedLifecycle>()) reg.As<INeedLifecycle>();

			// set parameters
			foreach (var pair in parameters) 
				reg.WithProperty(pair.Key, pair.Value);

			// this service is a singleton instance
			reg.SingleInstance();
		}

		public static void RegisterView<TViewModel, TView>() {
			Builder.RegisterType<TViewModel>().As<TViewModel>();
			Builder.RegisterType<TView>().As<TView>();
			_vmToViewMap[typeof(TViewModel)] = typeof(TView);
		}

		public static object ResolveView(Type vmType) {
			return Resolver.ResolveView(vmType);
		}

		public static TSvc Resolve<TSvc>()
			where TSvc : class
		{
			return Resolver.Resolve<TSvc>();
		}

		public static void Reset() {
			if (_scope != null) {
				_scope.Dispose();
				_scope = null;
			}
			if (_container != null) {
				_container.Dispose();
				_container = null;
			}
			_resolver = null; // already disposed with scope
			_builder = null;

			Builder.RegisterType<ResolverImpl>()
				.As<IResolverService>()
				.InstancePerDependency();
		}

		class ResolverImpl : IResolverService, IDisposable {

			ILifetimeScope _scope;

			public ResolverImpl() {
				_scope = Ioc.Scope.BeginLifetimeScope();
			}

			/// <inheritdoc/>
			public object ResolveView(Type vmType) 
			{
				if (!Ioc._vmToViewMap.ContainsKey(vmType))
					throw new InvalidOperationException(
						string.Format("No view type registered for view model type {0}", vmType.Name)
					);
				return _scope.Resolve(_vmToViewMap[vmType]);
			}

			/// <inheritdoc/>
			public TSvc Resolve<TSvc>() 
				where TSvc : class
			{
				return _scope.Resolve<TSvc>();
			}

			public void Dispose() {
				if (_scope != null) {
					_scope.Dispose();
					_scope = null;
				}
			}
		}
	}
}

