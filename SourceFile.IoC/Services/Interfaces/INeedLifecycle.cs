﻿using System;

namespace SourceFile.IoC.ServiceInterfaces
{
	/// <summary>
	/// Base interface for all "life cycle interfaces" that are served with
	/// life cycle events by the <see cref="ILifecycleService"/>
	/// </summary>
	public interface INeedLifecycle
	{
	}
}

