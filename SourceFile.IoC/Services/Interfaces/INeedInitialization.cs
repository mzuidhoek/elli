﻿using System;

namespace SourceFile.IoC.ServiceInterfaces
{
	public interface INeedInitialization : INeedLifecycle
	{
		void Initialize();
	}
}

