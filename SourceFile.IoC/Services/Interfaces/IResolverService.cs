﻿using System;

namespace SourceFile.IoC.ServiceInterfaces
{
	public interface IResolverService
	{
		object ResolveView(Type vmType);

		TSvc Resolve<TSvc>() where TSvc : class; 
	}
}

