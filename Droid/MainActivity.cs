﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using SourceFile.IoC;
using Xamarin.Forms;
using SourceFile.IoC.UI.Interfaces;
using ELLI.ViewModels;

namespace ELLI.Droid
{
	[Activity (Label = "ELLI.Droid", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);

			LoadApplication (new App (new Lazy<Page>(() =>
				{
					var nav = Ioc.Resolve<INavigationService>();
					return nav.CreateView<MainViewModel>() as Page;
				})));
		}
	}
}

