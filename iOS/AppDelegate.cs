﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Xamarin.Forms;
using SourceFile.IoC.UI.Interfaces;
using SourceFile.IoC;
using ELLI.ViewModels;

namespace ELLI.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			// Code for starting up the Xamarin Test Cloud Agent
			#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start();
			#endif

			LoadApplication (new App (new Lazy<Page>(() =>
				{
					var nav = Ioc.Resolve<INavigationService>();
					return nav.CreateView<MainViewModel>() as Page;
				})));

			return base.FinishedLaunching (app, options);
		}
	}
}

